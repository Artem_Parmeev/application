package com.example.kursa43;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.kursa43.constants.DBConstants;
import com.google.android.material.navigation.NavigationView;

/**
 * Класс, описывающий главную активноть приложения
 *
 * @author Parmeev A.V. 17IT17
 */
public class MainActivity extends AppCompatActivity implements MenuItem.OnMenuItemClickListener, DBConstants {

    private AppBarConfiguration mAppBarConfiguration;
    private NavController navController;
    private View header;
    private SharedPreferences prefs;
    public static boolean isAccess = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefs = getSharedPreferences("AccData", MODE_PRIVATE);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_library, R.id.nav_authorization, R.id.nav_registration, R.id.nav_bookmarks)
                .setDrawerLayout(drawer)
                .build();
        header = navigationView.getHeaderView(0);
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        setHeaderName();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (prefs.contains(DATABASE_EMAIL)) {
            getMenuInflater().inflate(R.menu.log_out, menu);
            MenuItem item = menu.findItem(R.id.log_out);
            item.setOnMenuItemClickListener(this);
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    /**
     * Устанавливает фрагмент по id
     *
     * @param setId id
     */
    public void setFragment(int setId) {
        navController.navigate(setId);
    }

    /**
     * Записывает в заголовок email и имя
     */
    @SuppressLint("StringFormatInvalid")
    public void setHeaderName() {
        TextView userDataTV = header.findViewById(R.id.userInfo);
        String name = prefs.getString(DATABASE_NAME, null);
        String email = prefs.getString(DATABASE_EMAIL, null);
        if (name != null && email != null) {
            String header = getApplication().getString(R.string.nav_header_title);
            userDataTV.setText(header + "\n" + name + " \n" + email);
            isAccess = true;
        }
    }

    /**
     * Очищает заголовок
     */
    public void clearHeader() {
        TextView userDataTv = header.findViewById(R.id.userInfo);
        userDataTv.setText(null);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        DialogueBox dialogueBox = new DialogueBox(this);
        dialogueBox.show(getSupportFragmentManager(), null);
        return true;
    }
}
