package com.example.kursa43;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.kursa43.constants.DBConstants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Класс, представляющий собой базу данных, содержащая
 * в себе некоторые таблицы
 *
 * @author Parmeev A.V. 17IT17
 */
public class DataBaseHelper extends SQLiteOpenHelper implements DBConstants {
    private Context context;
    private AssetManager assetManager;

    public DataBaseHelper(@Nullable Context context) {
        super(context, "library", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        assetManager = context.getAssets();
        db.execSQL("create table " + DATABASE_CLIENTS + " (" + DATABASE_ID + " integer primary key autoincrement,"
                + DATABASE_NAME + " text,"
                + DATABASE_EMAIL + " text,"
                + DATABASE_PASSWORD + " text" + ");");

        db.execSQL("create table " + DATABASE_BOOKS + " (" + DATABASE_ID + " integer primary key autoincrement,"
                + DATABASE_BOOK_NAME + " text,"
                + DATABASE_AUTHOR + " text,"
                + DATABASE_GENRE + " text,"
                + "releaseDate text,"
                + "page text,"
                + DATABASE_PATH_TO_IMAGE + " text" + ");");

        db.execSQL("create table " + DATABASE_BOOKMARKS + " (" + DATABASE_ID + " integer NOT NULL,"
                + DATABASE_EMAIL + " text NOT NULL"
                + ")");
        initBookList(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * Инициализирует таблицу автомобилей
     *
     * @param db база данных
     */
    private void initBookList(SQLiteDatabase db) {
        try {
            Cursor cursor = db.query(DATABASE_BOOKS, null,
                    null, null, null,
                    null, null);
            ContentValues values = new ContentValues();
            ArrayList<String> names = getData("names");
            ArrayList<String> authors = getData("authors");
            ArrayList<String> genres = getData("genres");
            ArrayList<String> paths = getData("paths");
            ArrayList<String> releaseDates = getData("releaseDates");
            ArrayList<String> pages = getData("pages");
            for (int i = 0; i < 10; i++) {
                values.put(DATABASE_BOOK_NAME, names.get(i));
                values.put(DATABASE_AUTHOR, authors.get(i));
                values.put(DATABASE_GENRE, genres.get(i));
                values.put(DATABASE_PATH_TO_IMAGE, paths.get(i));
                values.put("releaseDate", releaseDates.get(i));
                values.put("page", pages.get(i));
                db.insert(DATABASE_BOOKS, null, values);
            }
            cursor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Читает и сохраняет текстовые данные для
     * таблицы автомобилей из текстового файла
     *
     * @return списочный массив, в каждом
     * элементе которого хранится 1 строка текстового файла
     * @throws IOException исключение, выбрасываемое при ошибке
     *                     чтения или записи на файл
     */
    private ArrayList<String> getData(String fileName) throws IOException {
        ArrayList<String> tmp = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(assetManager.open(fileName + ".txt")));
        String line;
        while ((line = reader.readLine()) != null) {
            tmp.add(line);
        }
        reader.close();
        return tmp;
    }
}
