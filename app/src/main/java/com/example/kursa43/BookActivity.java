package com.example.kursa43;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.kursa43.constants.DBConstants;

/**
 * Класс, представляющий собой активность, которая
 * отображает подробные данные о выбранной книге
 *
 * @author Parmeev A.V. 17IT17
 */
public class BookActivity extends AppCompatActivity implements View.OnClickListener, DBConstants {

    private SQLiteDatabase db;
    private int arg;
    private Button descriptionBtn;
    private Button bookmarksBtn;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        email = getSharedPreferences("AccData", Context.MODE_PRIVATE).getString(DATABASE_EMAIL, null);
        ImageView imageView = findViewById(R.id.image_model_big);
        TextView textView = findViewById(R.id.data);
        descriptionBtn = findViewById(R.id.btn_for_description);
        bookmarksBtn = findViewById(R.id.btn_to_bookmark);
        descriptionBtn.setOnClickListener(this);
        bookmarksBtn.setOnClickListener(this);
        Intent intent = getIntent();
        DataBaseHelper dbHelper = new DataBaseHelper(this);
        db = dbHelper.getWritableDatabase();
        arg = intent.getIntExtra("id", 0);
        String[] data = getApplication().getResources().getStringArray(R.array.data);
        Cursor cursor = db.rawQuery("select * from " + DATABASE_BOOKS + " where " + DATABASE_ID + " = " + arg, null);
        if (cursor.moveToFirst()) {
            String dataTextView;
            String tmp;
            for (int i = 1; i < cursor.getColumnCount(); i++) {
                dataTextView = textView.getText().toString();
                tmp = cursor.getString(cursor.getColumnIndex(cursor.getColumnNames()[i]));
                if (i == 1) {
                    setTitle(tmp);
                }
                if (i < 6) {
                    updateText(textView, dataTextView, data[i - 1], tmp);
                }
                setImage(imageView, tmp);
            }
        }
        if (checkBookmarks()) {
            setColor();
        }
        cursor.close();
    }



    private boolean checkBookmarks() {
        Cursor cursor = db.rawQuery("select * from " + DATABASE_BOOKMARKS, null);
        return isFoundSame(cursor, email, arg) | email == null;
    }

    private void setColor() {
        bookmarksBtn.setBackgroundColor(getResources().getColor(R.color.grey));
        bookmarksBtn.setTextColor(getResources().getColor(R.color.bright_grey));
        bookmarksBtn.setText("В закладках");
    }

    @Override
    public void onClick(View v) {
        if (descriptionBtn.equals(v)) {
            DescriptionBox descriptionBox = new DescriptionBox(this, arg);
            descriptionBox.show(getSupportFragmentManager(), null);
        } else {
            if (email == null) {
                return;
            }
            Cursor cursor = db.rawQuery("select * from " + DATABASE_BOOKMARKS, null);
            if (!isFoundSame(cursor, email, arg)) {
                ContentValues values = new ContentValues();
                values.put(DATABASE_ID, arg);
                values.put(DATABASE_EMAIL, email);

                db.insertWithOnConflict(DATABASE_BOOKMARKS, null, values, SQLiteDatabase.CONFLICT_NONE);
                Toast.makeText(this, R.string.add, Toast.LENGTH_SHORT).show();
                setColor();
            }
        }
    }

    /**
     * Обновляет данные текстового поля
     *
     * @param textView     текстовое поле
     * @param dataTextView данные текстового поля
     * @param parameter    строка, относительно которой сформируется
     *                     добавляемый текст
     * @param data         добавляемые данные
     */
    private void updateText(TextView textView, String dataTextView, String parameter, String
            data) {
        String tmp = String.format(parameter, data);
        dataTextView = dataTextView.concat(tmp);
        textView.setText(dataTextView);
    }

    /**
     * Устанавливает изображение в интерфейсный компонент
     * ImageView
     *
     * @param imageView интерфейсный компонент, отображающий
     *                  изображение
     * @param path      путь к изображению
     */
    private void setImage(ImageView imageView, String path) {
            Glide.with(getApplicationContext()).load(Uri.parse("file:///android_asset/images/" + path)).into(imageView);
    }

    /**
     * Метод, который проверяет на наличие текущей модели
     * автомобиля в корзине пользователя
     *
     * @param cursor курсор, отбирающий данные из таблицы по запросу
     * @param email  электронная почта пользователя
     * @param arg    аргумент, представляет собой id автомобиля
     * @return true если совпадение нашлось иначе false
     */
    private boolean isFoundSame(Cursor cursor, String email, int arg) {
        boolean flag = false;
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(DATABASE_ID);
            int emailIndex = cursor.getColumnIndex(DATABASE_EMAIL);
            do {
                if (cursor.getInt(idIndex) == arg && cursor.getString(emailIndex).equals(email)) {
                    flag = true;
                    break;
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return flag;
    }
}
