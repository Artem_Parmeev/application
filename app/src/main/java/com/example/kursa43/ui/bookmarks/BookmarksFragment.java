package com.example.kursa43.ui.bookmarks;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kursa43.BookmarkActivity;
import com.example.kursa43.DataBaseHelper;
import com.example.kursa43.LibraryAdapter;
import com.example.kursa43.R;
import com.example.kursa43.constants.DBConstants;

/**
 * Класс, описывающий фрагмент,
 * отображающий сохраненные пользователем,
 * книги
 *
 * @author Parmeev A.V. 17IT17
 */
public class BookmarksFragment extends Fragment implements SearchView.OnQueryTextListener, DBConstants {

    private SQLiteDatabase db;
    private String query;
    private View root;
    private LibraryAdapter libraryAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_bookmarks, container, false);

        initBookmarks(root);

        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search, menu);
        MenuItem menuItem = menu.findItem(R.id.search);

        androidx.appcompat.widget.SearchView searchView = (androidx.appcompat.widget.SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        initBookmarks(root);
    }

    /**
     * Инициализирует компоненты фрагмента
     *
     * @param root базовый интерфейсный блок
     */
    private void initBookmarks(View root) {
        DataBaseHelper dbHelper = new DataBaseHelper(root.getContext());
        db = dbHelper.getWritableDatabase();
        String email = root.getContext().getSharedPreferences("AccData", Context.MODE_PRIVATE).getString(DATABASE_EMAIL, null);
        if (email != null) {
            initData(email);
            RecyclerView recyclerView = root.findViewById(R.id.bookmarks);
            LinearLayoutManager manager = new LinearLayoutManager(root.getContext());
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(manager);
            libraryAdapter = new LibraryAdapter(root.getContext(), db, query, null, new BookmarkActivity());
            recyclerView.setAdapter(libraryAdapter);
        }
    }

    /**
     * Инициализирует данные для последующего отображения
     * в закладках авторизованного пользователя
     *
     * @param email эл. почта авторизованного пользователя
     */
    private void initData(String email) {
        Cursor cursor = db.query(DATABASE_BOOKMARKS, new String[]{DATABASE_ID}, DATABASE_EMAIL + " = ?", new String[]{email}, null, null, null);
        int[] arrId = new int[cursor.getCount()];
        if (cursor.moveToFirst()) {
            int id = cursor.getColumnIndex(DATABASE_ID);
            int i = 0;
            do {
                arrId[i] = cursor.getInt(id);
                i++;
            } while (cursor.moveToNext());
        }
        cursor.close();

        String args = getArgs(arrId);
        query = "select * from " + DATABASE_BOOKS + " where " + DATABASE_ID + " = " + args;
    }

    /**
     * Формирует строку аргументов для
     * запроса на языке SQL для таблицы базы данных
     *
     * @param arrId массив целочисленных аргументов
     * @return строку аргументов
     */
    private String getArgs(int[] arrId) {
        if (arrId.length == 0) {
            return "0";
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < arrId.length; i++) {
            if (i == arrId.length - 1) {
                builder.append(arrId[i]);
            } else {
                builder.append(arrId[i]).append(" or " + DATABASE_ID + " = ");
            }
        }
        return builder.toString();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        libraryAdapter.getFilter().filter(newText);
        return false;
    }
}
