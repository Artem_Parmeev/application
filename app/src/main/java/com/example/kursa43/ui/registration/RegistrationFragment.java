package com.example.kursa43.ui.registration;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.kursa43.DataBaseHelper;
import com.example.kursa43.MainActivity;
import com.example.kursa43.R;
import com.example.kursa43.constants.DBConstants;

/**
 * Класс, описывающий фрагмент,
 * отображающий окно регистрации
 *
 * @author Parmeev A.V. 17IT17
 */
public class RegistrationFragment extends Fragment implements View.OnClickListener, DBConstants {

    private EditText inputEmail;
    private EditText inputName;
    private EditText inputPassword;
    private EditText inputRepeatPassword;
    private SQLiteDatabase db;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_registration, container, false);

        initialize(root);
        return root;

    }

    /**
     * Инициализирует интерфейсные компоненты фрагмента
     *
     * @param root базовый интерфейсный блок
     */
    private void initialize(View root) {
        DataBaseHelper dbHelper = new DataBaseHelper(root.getContext());
        db = dbHelper.getWritableDatabase();
        inputEmail = root.findViewById(R.id.inputEmail);
        inputName = root.findViewById(R.id.inputName);
        inputPassword = root.findViewById(R.id.inputPassword);
        inputRepeatPassword = root.findViewById(R.id.inputPasswordRepeat);
        Button btnRegistration = root.findViewById(R.id.btnRegistration);
        btnRegistration.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String email = inputEmail.getText().toString();
        String name = inputName.getText().toString();
        String password = inputPassword.getText().toString();
        String repeatPassword = inputRepeatPassword.getText().toString();
        if (isNotValidData(email, name, password, repeatPassword)) {
            return;
        }
        if (isFreeAcc(email)) {
            ContentValues values = new ContentValues();
            values.put(DATABASE_EMAIL, email);
            values.put(DATABASE_NAME, name);
            values.put(DATABASE_PASSWORD, password);
            db.insert(DATABASE_CLIENTS, null, values);
            inputEmail.setText(null);
            inputName.setText(null);
            inputPassword.setText(null);
            inputRepeatPassword.setText(null);
            Toast.makeText(v.getContext(), "Регистрация прошла успешно",
                    Toast.LENGTH_LONG).show();
            MainActivity activity = (MainActivity) getActivity();
            if (activity != null) {
                activity.setFragment(R.id.nav_authorization);
            }
        } else {
            setError(inputEmail, "Почтовый ящик занят");
        }
    }


    /**
     * Возвращает true, если данные верны,
     * иначе false
     *
     * @param email          почта
     * @param name           имя
     * @param password       пароль
     * @param repeatPassword повторенный пароль
     * @return true или false
     */
    private boolean isNotValidData(String email, String name, String password, String repeatPassword) {
        if (!email.contains("@") || email.contains(" ")) {
            setError(inputEmail, "Неверный формат email");
            return true;
        }
        if (name.isEmpty()) {
            setError(inputName, "Поле не должно быть пустым");
            return true;
        }
        if (password.length() < 5) {
            setError(inputPassword, "Кол-во символов меньше 5");
            return true;
        }
        if (!password.equals(repeatPassword)) {
            setError(inputRepeatPassword, "Пароли не сопадают");
            return true;
        }
        return false;
    }

    /**
     * Устанавливает сообщение об ошибке
     * в поле ввода
     *
     * @param field   поле ввода
     * @param message сообщение
     */
    private void setError(EditText field, String message) {
        field.setError(message);
    }

    /**
     * Проверяет занятость почты
     *
     * @param email почты
     * @return true или false
     */
    private boolean isFreeAcc(String email) {
        Cursor cursor = db.query(DATABASE_CLIENTS, new String[]{DATABASE_EMAIL}, null,
                null, null,
                null, null);
        if (cursor.moveToFirst()) {
            int emailIndex = cursor.getColumnIndex(DATABASE_EMAIL);
            do {
                if (email.equals(cursor.getString(emailIndex))) {
                    return false;
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return true;
    }
}