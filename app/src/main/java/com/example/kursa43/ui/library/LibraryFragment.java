package com.example.kursa43.ui.library;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kursa43.BookActivity;
import com.example.kursa43.DataBaseHelper;
import com.example.kursa43.LibraryAdapter;
import com.example.kursa43.R;
import com.example.kursa43.constants.DBConstants;

/**
 * Класс, описывающий фрагмент,
 * отображающий библиотеку
 *
 * @author Parmeev A.V. 17IT17
 */
public class LibraryFragment extends Fragment implements SearchView.OnQueryTextListener, DBConstants {

    private LibraryAdapter libraryAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_library, container, false);
        initBooksList(root);
        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search, menu);
        MenuItem menuItem = menu.findItem(R.id.search);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);
    }

    /**
     * Инициализирует компоненты фрагмента для отображения
     * книг
     *
     * @param root базовый интерфейсный блок
     */
    private void initBooksList(View root) {
        DataBaseHelper dbHelper = new DataBaseHelper(root.getContext());
        String query = "select " + DATABASE_ID + ", " + DATABASE_BOOK_NAME + ", " + DATABASE_AUTHOR + ", " + DATABASE_GENRE + ", " + DATABASE_PATH_TO_IMAGE + " from " + DATABASE_BOOKS;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        RecyclerView recyclerView = root.findViewById(R.id.books_list);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(root.getContext());
        recyclerView.setLayoutManager(layoutManager);
        libraryAdapter = new LibraryAdapter(root.getContext(), db, query, null, new BookActivity());
        recyclerView.setAdapter(libraryAdapter);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        libraryAdapter.getFilter().filter(newText);
        return false;
    }
}