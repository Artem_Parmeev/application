package com.example.kursa43.ui.authorization;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.kursa43.DataBaseHelper;
import com.example.kursa43.MainActivity;
import com.example.kursa43.R;
import com.example.kursa43.constants.DBConstants;

/**
 * Класс, представляющий собой фрагмент, отображающий
 * необходимые интерфейсные компоненты для авторизации
 * зарегистрированного пользователя
 *
 * @author Parmeev A.V. 17IT17
 */
public class AuthorizationFragment extends Fragment implements View.OnClickListener, DBConstants {

    private EditText inputEmail;
    private EditText inputPassword;
    private SQLiteDatabase db;
    private String userName;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_authorization, container, false);

        initialize(root);

        return root;
    }

    /**
     * Инициализирует интерфейсные компоненты
     * текущего фрагмента
     *
     * @param root базовый интерфейсный блок
     */
    private void initialize(View root) {
        DataBaseHelper dbHelper = new DataBaseHelper(root.getContext());
        db = dbHelper.getWritableDatabase();
        inputEmail = root.findViewById(R.id.inputEmail);
        inputPassword = root.findViewById(R.id.inputPassword);
        Button btnAuthorization = root.findViewById(R.id.btnAuthorization);
        btnAuthorization.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        String email = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();
        if (email.isEmpty()) {
            setError(inputEmail, "Пустое поле");
            return;
        }
        if (password.isEmpty()) {
            setError(inputPassword, "Пустое поле");
            return;
        }

        if (isSuccess(email, password)) {
            Toast.makeText(v.getContext(), "Успешная авторизация", Toast.LENGTH_LONG).show();
            inputEmail.setText(null);
            inputEmail.setText(null);
            SharedPreferences sp = v.getContext().getSharedPreferences("AccData", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(DATABASE_NAME, userName);
            editor.putString(DATABASE_EMAIL, email);
            editor.apply();

            MainActivity activity = (MainActivity) getActivity();
            if (activity != null) {
                activity.setHeaderName();
                activity.setFragment(R.id.nav_library);
            }
        } else {
            setError(inputEmail, "Неправильный email");
            setError(inputPassword, "Неправильный пароль");
        }

    }

    /**
     * Устанавливает сообщение об ошибке в поле ввода
     *
     * @param field   поле ввода
     * @param message сообщение
     */
    private void setError(EditText field, String message) {
        field.setError(message);
    }

    /**
     * Проверяет, корректные ли данные ввел пользователь
     * своего аккаунта
     *
     * @param email    эл. почта зарегистрированного пользователя
     * @param password пароль зарегистрированного пользователя
     * @return true если пользователь ввел данные своего аккаунта верно иначе false
     */
    private boolean isSuccess(String email, String password) {
        boolean flag = false;
        Cursor cursor = db.query(DATABASE_CLIENTS, null,
                null, null, null,
                null, null);
        if (cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(DATABASE_NAME);
            int emailIndex = cursor.getColumnIndex(DATABASE_EMAIL);
            int passwordIndex = cursor.getColumnIndex(DATABASE_PASSWORD);
            do {
                if (email.equals(cursor.getString(emailIndex)) && password.equals(cursor.getString(passwordIndex))) {
                    userName = cursor.getString(nameIndex);
                    flag = true;
                    break;
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return flag;
    }
}