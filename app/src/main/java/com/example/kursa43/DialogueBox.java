package com.example.kursa43;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

/**
 * Класс, описывающий диалоговое окно
 *
 * @author A.V.Parmeev 17IT17
 */
public class DialogueBox extends DialogFragment implements DialogInterface.OnClickListener {

    private Context context;

    DialogueBox(Context context) {
        super();
        this.context = context;
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        return builder
                .setTitle("Выход").setPositiveButton("Да", this)
                .setNegativeButton("Нет", null)
                .setMessage("Хотите выйти из аккаунта?").create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        SharedPreferences prefs = context.getSharedPreferences("AccData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) {
            activity.clearHeader();
            activity.setFragment(R.id.nav_authorization);
        }
    }
}
