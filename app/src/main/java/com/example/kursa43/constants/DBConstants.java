package com.example.kursa43.constants;

/**
 * Содержить названия таблиц и полей БД
 *
 * @author Parmeev A.V. 17IT17
 */
public interface DBConstants {
    String DATABASE_CLIENTS = "clients";
    String DATABASE_ID = "id";
    String DATABASE_NAME = "name";
    String DATABASE_EMAIL = "email";
    String DATABASE_PASSWORD = "password";

    String DATABASE_BOOKS = "books";
    String DATABASE_BOOK_NAME = "bookName";
    String DATABASE_AUTHOR = "author";
    String DATABASE_GENRE = "genre";
    String DATABASE_PATH_TO_IMAGE = "pathToImage";

    String DATABASE_BOOKMARKS = "bookmarks";
}
