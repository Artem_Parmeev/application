package com.example.kursa43;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.example.kursa43.constants.Descriptions;

/**
 * Класс, описывающий всплывающее окно
 * с описанием
 *
 * @author Parmeev A.V. 17IT17
 */
public class DescriptionBox extends DialogFragment implements Descriptions {
    private int arg;
    private Context context;

    DescriptionBox(Context context, int arg) {
        super();
        this.context = context;
        this.arg = arg;
    }

    @NonNull
    public Dialog onCreateDialog(Bundle SavedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        String description = getDescription(arg);
        return builder
                .setTitle("Описание")
                .setMessage(description).
                        setPositiveButton("Назад", null).create();
    }

    /**
     * Возвращает строку, содержащуюю
     * описание книги, взависимости от
     * числа
     *
     * @param number целое число
     * @return строку
     */
    private String getDescription(int number) {
        String txt = null;
        switch (number) {
            case 1:
                txt = FIRST_DESCRIPTION;
                break;
            case 2:
                txt = SECOND_DESCRIPTION;
                break;
            case 3:
                txt = THIRD_DESCRIPTION;
                break;
            case 4:
                txt = FOURTH_DESCRIPTION;
                break;
            case 5:
                txt = FIFTH_DESCRIPTION;
                break;
            case 6:
                txt = SIXTH_DESCRIPTION;
                break;
            case 7:
                txt = SEVENTH_DESCRIPTION;
                break;
            case 8:
                txt = EIGHTH_DESCRIPTION;
                break;
            case 9:
                txt = NINTH_DESCRIPTION;
                break;
            case 10:
                txt = TENTH_DESCRIPTION;
        }
        return txt;
    }
}
