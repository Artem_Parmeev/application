package com.example.kursa43;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.kursa43.constants.DBConstants;

import java.util.ArrayList;

/**
 * Класс, представляющий собой адаптер для интерфейсного
 * компонента RecyclerView и инициализирующий необходимые
 * данные для отображения библиотеки
 *
 * @author Parmeev A.V. 17IT17
 */
public class LibraryAdapter extends RecyclerView.Adapter<LibraryAdapter.BookHolder> implements Filterable, DBConstants {

    private SQLiteDatabase db;
    private Context context;
    private SparseArray<String> names;
    private SparseArray<String> authors;
    private SparseArray<String> genres;
    private ArrayList<Integer> ids;
    private ArrayList<Integer> idsFull;
    private SparseArray<String> imgs;
    private AppCompatActivity activity;
    private String[] args;
    private String query;

    public LibraryAdapter(Context context, SQLiteDatabase db, String query, String[] args, AppCompatActivity activity) {
        this.context = context;
        this.db = db;
        this.query = query;
        this.args = args;
        this.activity = activity;
        initData();
    }

    /**
     * Инициализирует необходимые данные для
     * их последующего отображения на экране пользователя
     */
    private void initData() {
        names = new SparseArray<>();
        authors = new SparseArray<>();
        genres = new SparseArray<>();
        ids = new ArrayList<>();
        imgs = new SparseArray<>();

        Cursor cursor = db.rawQuery(query, args);
        if (cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(DATABASE_BOOK_NAME);
            int authorIndex = cursor.getColumnIndex(DATABASE_AUTHOR);
            int genreIndex = cursor.getColumnIndex(DATABASE_GENRE);
            int idIndex = cursor.getColumnIndex(DATABASE_ID);
            int imgIndex = cursor.getColumnIndex(DATABASE_PATH_TO_IMAGE);
            int id;
            do {
                id = cursor.getInt(idIndex);
                names.put(id, cursor.getString(nameIndex));
                authors.put(id, cursor.getString(authorIndex));
                genres.put(id, cursor.getString(genreIndex));
                ids.add(id);
                imgs.put(id, cursor.getString(imgIndex));
            } while (cursor.moveToNext());
        }
        cursor.close();
        idsFull = new ArrayList<>(ids);
    }

    @Override
    public Filter getFilter() {
        return bookFilter;
    }

    private Filter bookFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ids.clear();
            if (constraint == null || constraint.length() == 0) {
                ids.addAll(idsFull);
            } else {
                int id;
                for (int i = 0; i < names.size(); i++) {
                    id = idsFull.get(i);
                    if (names.get(id).toLowerCase().trim().contains(constraint.toString().toLowerCase().trim())) {
                        ids.add(id);
                    }
                }
            }
            return null;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            notifyDataSetChanged();
        }
    };

    @NonNull
    @Override
    public BookHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_book, parent, false);
        return new LibraryAdapter.BookHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LibraryAdapter.BookHolder holder, int position) {
        position = ids.get(position);
        setImageFromPath(imgs.get(position), holder.img);
        setTextData(holder.bookName, names.get(position), R.string.book_name);
        setTextData(holder.author, authors.get(position), R.string.author_name);
        setTextData(holder.genre, genres.get(position), R.string.genre);
        holder.idData = position;
    }

    /**
     * Устанавливает изображение в интерфейсный компонент
     * ImageView при помощи библиотеки Glide
     *
     * @param path путь к изображению
     * @param img  интерфейсный компонент ImageView
     */
    private void setImageFromPath(String path, ImageView img) {
        Glide.with(context).load(Uri.parse("file:///android_asset/images/" + path)).into(img);
    }

    /**
     * Изменяет содержимое текстового поля
     *
     * @param textView текстовое поле
     * @param string   строка, которую необходимо добавить
     * @param idRes    строковый ресурс, относительно которого форматируется строка
     */
    private void setTextData(TextView textView, String string, int idRes) {
        String tmp = context.getString(idRes);
        textView.setText(string);
    }

    @Override
    public int getItemCount() {
        return ids.size();
    }

    public class BookHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Integer idData;
        private TextView bookName;
        private TextView author;
        private TextView genre;
        private ImageView img;

        BookHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.image_model);
            bookName = itemView.findViewById(R.id.book_name);
            author = itemView.findViewById(R.id.author_name);
            genre = itemView.findViewById(R.id.genre_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, activity.getClass());
            intent.putExtra(DATABASE_ID, idData);
            context.startActivity(intent);
        }
    }
}


